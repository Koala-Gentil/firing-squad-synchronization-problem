#include "Solution.h"
#include <cassert>

std::array<int, NB_FREE_RULES> Solution::_freeRulesIndex;
std::array<int, NB_RULES> Solution::_rulesTemplate;

Solution::Solution()
    : _rules(_rulesTemplate), _fitness(-1)
{ }

void Solution::randomize(std::mt19937& generator) {
    std::uniform_int_distribution<int> distribution {GEN, FIRE-1};
    for(int ruleIndex: _freeRulesIndex) {
        _rules[ruleIndex] = distribution(generator);
    }
}

void Solution::randomWalk(std::mt19937 &generator) {
    std::uniform_int_distribution<int> ruleDistribution {0, _freeRulesIndex.size() - 1};
    std::uniform_int_distribution<int> stateDistribution {GEN, FIRE-1};

    _rules[_freeRulesIndex[ruleDistribution(generator)]] = stateDistribution(generator);
}

void Solution::combine(const Solution &other, std::mt19937 &generator) {
    std::uniform_int_distribution<int> boolDistribution {0, 1};
    for(int ruleIndex: _freeRulesIndex) {
        if(boolDistribution(generator)) {
            _rules[ruleIndex] = other._rules[ruleIndex];
        }
    }
}



void Solution::fitness(int value) {
    _fitness = value;
}

int Solution::fitness() const {
    return _fitness;
}

std::array<int, NB_RULES> Solution::rules() {
    return _rules;
}

void Solution::disturb(std::mt19937 &generator, int nbRules) {
    std::uniform_int_distribution<int> statesDistribution {GEN, FIRE-1};
    std::uniform_int_distribution<int> rulesDistribution {0, _freeRulesIndex.size() - 1};

    for(int i = 0; i < nbRules; ++i) {
        _rules[_freeRulesIndex[rulesDistribution(generator)]] = statesDistribution(generator);
    }
}

void Solution::getNeighbors(std::array<Solution, NB_NEIGHBORS>& neigbhors) {
    int i = 0;
    for(int ruleIndex: _freeRulesIndex) {
        for(int n = 0; n < FIRE; ++n) {
            if(n == _rules[ruleIndex]) continue;

            Solution& s = neigbhors[i++] = *this;
            s._fitness = -1;
            s._rules[ruleIndex] = n;
        }
    }
}

int Solution::getRuleIndex(int l, int c, int r) {
    assert(l >= 0 && l < NB_DIGITS && c >= 0 && c < NB_DIGITS && r >= 0 && r < NB_DIGITS);
    return l * NB_DIGITS_2 + c * NB_DIGITS + r;
}

void Solution::setRule(int l, int c, int r, int value) {
    assert(l >= 0 && l < NB_DIGITS && c >= 0 && c < NB_DIGITS && r >= 0 && r < NB_DIGITS);

    _rules[getRuleIndex(l,c,r)] = value;
}

int Solution::getRule(int l, int c, int r) const {
    return _rules[getRuleIndex(l,c,r)];
}

// STATIC
void Solution::init() {
    calcFreeRules();
    initRulesTemplate();
}

void Solution::calcFreeRules() {
    int i = 0;
    for(int l = 0; l < FIRE; ++l) {
        for(int m = 0; m < FIRE; ++m) {
            for(int r = 0; r < FIRE; ++r) {
                if(l == 0 && m == 0 && r == 0) continue;
                if(l == 1 && m == 1 && r == 1) continue;
                if(l == 1 && m == 0 && r == 0) continue;
                if(l == 2 && m == 0 && r == 0) continue;
                _freeRulesIndex[i++] = getRuleIndex(l, m, r);
            }
        }
    }
    for(int m = 0; m < FIRE; ++m) {
        for(int r = 0; r < FIRE; ++r) {
            if(m == 0 && r == 0) continue;
            if(m == 1 && r == 1) continue;
            if(m == 1 && r == 0) continue;
            _freeRulesIndex[i++] = getRuleIndex(BORD, m, r);
        }
    }
    for(int l = 0; l < FIRE; ++l) {
        for (int m = 0; m < FIRE; ++m) {
            if (m == 0 && l == 0) continue;
            if (m == 1 && l == 1) continue;
            if (m == 0 && l == 1) continue;
            if (m == 0 && l == 2) continue;
            _freeRulesIndex[i++] = getRuleIndex(l, m, BORD);
        }
    }
}

void Solution::initRulesTemplate() {
    for(int& rule: _rulesTemplate)
        rule = IMPOSSIBLE;

    for(int ruleIndex: _freeRulesIndex)
        _rulesTemplate[ruleIndex] = UNUSED;

    _rulesTemplate[getRuleIndex(REPOS, REPOS, REPOS)] = REPOS;
    _rulesTemplate[getRuleIndex(BORD, REPOS, REPOS)] = REPOS;
    _rulesTemplate[getRuleIndex(REPOS, REPOS, BORD)] = REPOS;
    _rulesTemplate[getRuleIndex(GEN, GEN, GEN)] = FIRE;
    _rulesTemplate[getRuleIndex(BORD, GEN, GEN)] = FIRE;
    _rulesTemplate[getRuleIndex(GEN, GEN, BORD)] = FIRE;
    _rulesTemplate[getRuleIndex(BORD, GEN, REPOS)] = GEN;
    _rulesTemplate[getRuleIndex(GEN, REPOS, BORD)] = GEN;

    // diagonal signal
    _rulesTemplate[getRuleIndex(GEN, REPOS, REPOS)] = GEN+1;
    _rulesTemplate[getRuleIndex(GEN+1, REPOS, REPOS)] = GEN;

    // for success length 3
    _rulesTemplate[getRuleIndex(GEN+1, REPOS, BORD)] = GEN+1;
}

// IO
void Solution::print() {
    for(int l = 0; l < NB_DIGITS; ++l) {
        for(int c = 0; c < NB_DIGITS; ++c) {
            for(int r = 0; r < NB_DIGITS; ++r) {
                std::cout << l << c << r << " -> " << getRule(l,c,r) << std::endl;
            }
        }
    }
}

std::ostream &operator<<(std::ostream &os, const Solution &solution) {
    for(int i = 0; i < NB_RULES; i++) {
        os << solution._rules[i];
    }
    return os;
}

std::istream &operator>>(std::istream &is, Solution &solution) {
    char c;
    for(int i = 0; i < NB_RULES; i++) {
        is >> c;
        solution._rules[i] = c - '0';
    }
    return is;
}