#include "Automata.h"

Automata::Automata() {

}

void Automata::calc(Solution &solution) {
    bool failed = false;
    unsigned N = 2;
    while (!failed && N < AUTOMATA_MAX_SIZE) {
        init(N);
        const int nbIteration = 2 * N - 2;
        for(int n = 1; n <= nbIteration; ++n) {
            int pl = _state[0];
            for(int i = 1; i <= N; ++i) {
                int value = solution.getRule(pl, _state[i], _state[i+1]);
                if(value < 0 || value > FIRE) {
                    std::cout << "ERREUR" << std::endl;
                }
                pl = _state[i];
                _state[i] = value;
                if(value == FIRE && n < nbIteration) {
                    failed = true;
                    break;
                }
            }
            if(failed)
                break;
        }
        for(int i = 1; i <= N; ++i) {
            if(_state[i] != FIRE) {
                failed = true;
                break;
            }
        }
        if(!failed) {
            N++;
        }
    }
    solution.fitness(N-1);
}

void Automata::init(unsigned N) {
    _state[0] = BORD;
    _state[N + 1] = BORD;
    _state[1] = GEN;
    for(int i = 2; i <= N ; ++i) {
        _state[i] = REPOS;
    }
}

void Automata::step(const Solution &solution, unsigned N) {
    int pl = _state[0];
    for(int i = 1; i < N +1; ++i) {
        int value = solution.getRule(pl, _state[i], _state[i+1]);
        if(value > FIRE) value = 0;
        pl = _state[i];
        _state[i] = value;
    }
}

std::vector<std::vector<int>> Automata::getTrace(const Solution& solution, unsigned N) {
    const int nbIteration = 2 * N - 2;

    std::vector<std::vector<int>> trace(nbIteration+1, std::vector<int>(N, 0));
    init(N);
    for(int i = 0; i < N; ++i) {
        trace[0][i] = _state[i+1];
    }
    for(int n = 1; n <= nbIteration; ++n) {
        step(solution, N);
        for(int i = 0; i < N; ++i) {
            trace[n][i] = _state[i+1];
        }
    }

    return trace;
}
