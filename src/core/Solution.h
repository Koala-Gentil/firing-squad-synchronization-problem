#ifndef FSSP_SOLUTION_H
#define FSSP_SOLUTION_H

#include <array>
#include <cmath>
#include <iostream>
#include <random>
#include "constantes.h"

class Solution {
public:
    Solution();
    void fitness(int value);
    int fitness() const;
    std::array<int, NB_RULES> rules();
    void getNeighbors(std::array<Solution, NB_NEIGHBORS>& neigbhors);

    int getRule(int l, int c, int r) const;
    void setRule(int l, int c, int r, int value);
    void randomize(std::mt19937& generator);
    void disturb(std::mt19937& generator, int nbRules);
    void combine(const Solution& other, std::mt19937& generator);
    void randomWalk(std::mt19937& generator);
    void print();

private:
    std::array<int, NB_RULES> _rules;
    int _fitness;

public:
    static void init(); // required

private:
    static inline int getRuleIndex(int l, int c, int r);
    static void calcFreeRules();
    static void initRulesTemplate();

    static std::array<int, NB_FREE_RULES> _freeRulesIndex;
    static std::array<int, NB_RULES>  _rulesTemplate;

    friend std::ostream& operator<<(std::ostream& os, const Solution& solution);
    friend std::istream& operator>>(std::istream& is, Solution& solution);
};

std::ostream& operator<<(std::ostream& os, const Solution& solution);
std::istream& operator>>(std::istream& is, Solution& solution);

#endif //FSSP_SOLUTION_H
