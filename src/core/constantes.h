#ifndef FSSP_PARAMS_H
#define FSSP_PARAMS_H

#include <cmath>

// For GCC
constexpr int64_t ipow(int64_t base, int exp, int64_t result = 1) {
    return exp < 1 ? result : ipow(base*base, exp/2, (exp % 2) ? result*base : result);
}

const int NB_STATES = 5;
const int NB_DIGITS = NB_STATES + 1;
const int NB_DIGITS_2 = ipow(NB_DIGITS, 2);
const int NB_RULES = NB_DIGITS * NB_DIGITS * NB_DIGITS;
const int REPOS = 0;
const int GEN = 1;
const int FIRE = NB_STATES - 1;
const int BORD = NB_STATES;
const int UNUSED = NB_STATES + 1;
const int IMPOSSIBLE = NB_STATES + 2;
const int NB_FREE_RULES = ipow(NB_STATES - 1, 3) + 2 * ipow(NB_STATES - 1, 2) - 11;
const int NB_NEIGHBORS = (NB_STATES-2) * NB_FREE_RULES;
const int AUTOMATA_MAX_SIZE = 30;

#endif //FSSP_PARAMS_H
