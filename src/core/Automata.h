#ifndef FSSP_AUTOMATA_H
#define FSSP_AUTOMATA_H

#include <array>
#include <vector>
#include "constantes.h"
#include "Solution.h"

class Automata {
public:
    Automata();
    void calc(Solution& solution);

    std::vector<std::vector<int>> getTrace(const Solution& solution, unsigned N);

private:
    void step(const Solution &solution, unsigned N);
    void init(unsigned N);

    std::array<int, AUTOMATA_MAX_SIZE + 2> _state;
};


#endif //FSSP_AUTOMATA_H
