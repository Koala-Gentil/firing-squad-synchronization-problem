//
// Created by koala on 12/12/18.
//

#ifndef FSSP_SVGEXPORTER_H
#define FSSP_SVGEXPORTER_H

#include "core/Solution.h"
#include "core/Automata.h"
#include <fstream>

class SvgExporter {
public:
    SvgExporter();
    static void print(const Solution& s, Automata& automata, unsigned N, std::string fileName, bool all = false);

private:
    static std::string getStateColor(int state);
    static void cellSVG(std::fstream& file, int x, int y, int state);
    static void headSVG(std::fstream& file, int w, int h);

    static constexpr int width = 10;
    static constexpr int height = 10;
};


#endif //FSSP_SVGEXPORTER_H
