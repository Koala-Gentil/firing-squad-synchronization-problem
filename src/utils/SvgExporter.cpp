#include "SvgExporter.h"

SvgExporter::SvgExporter() {

}

void SvgExporter::print(const Solution &s, Automata &automata, unsigned N, std::string fileName, bool all) {
    std::fstream file(fileName, std::ios::out);

    if (file) {
        unsigned nInit;

        if (all) {
            nInit = 2;
            headSVG(file, (N * (N + 1) / 2 + (N - 1) * 2) * width, (2 * N) * height);
        } else {
            nInit = N;
            headSVG(file, (N + 1) * width, (2 * N) * height);
        }

        int dx = 0;
        for(unsigned n = nInit; n <= N; n++) {
            auto trace = automata.getTrace(s, n);
            for(int i = 0; i < trace.size(); ++i) {
                for(int j = 0; j < trace[i].size(); ++j) {
                   cellSVG(file, dx + j * width, i * height, trace[i][j]);
                }
            }
            dx += (n+2) * width;
        }

        // bottom of file
        file << "</g> </svg>" << std::endl;

        file.close();
    } else
        std::cerr << "print: impossible to open file " << fileName << std::endl;
}

std::string SvgExporter::getStateColor(int state) {
    if (state > FIRE)
        return "grey";
    else if (state == REPOS)
        return "white";
    else if (state == FIRE)
        return "red" ;
    else if (state == GEN)
        return "blue";
    else if (state == GEN + 1)
        return "yellow";
    else if (state == GEN + 2)
        return "green";
    else if (state == GEN + 3)
        return "violet";
    else if (state == GEN + 4)
        return "orange";
    else
        return "grey";
}

void SvgExporter::cellSVG(std::fstream &file, int x, int y, int state) {
    file << "<rect "
         << "width=\""  << width   << "\" "
         << "height=\"" << height  << "\" "
         << "x=\"" << (x + width)  << "\" "
         << "y=\"" << (y + height) << "\" "
         << "fill=\"" << getStateColor(state) << "\"/>"
         << std::endl;
}

void SvgExporter::headSVG(std::fstream &file, int w, int h) {
    file << "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
         << std::endl
         << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">"
         << std::endl << std::endl
         << "<svg" << std::endl
         << "xmlns=\"http://www.w3.org/2000/svg\"" << std::endl
         << "xmlns:xlink=\"http://www.w3.org/1999/xlink\"" << std::endl
         << "xmlns:ev=\"http://www.w3.org/2001/xml-events\"" << std::endl
         << "version=\"1.1\"" << std::endl
         << "baseProfile=\"full\"" << std::endl
         << "width=\"" << w <<"\" height=\"" << h << "\">" << std::endl // viewBox="0 0 800 400">
         << "<g stroke-width=\"1px\" stroke=\"black\" fill=\"white\">" << std::endl;
}
