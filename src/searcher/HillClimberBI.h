//
// Created by koala on 13/12/18.
//

#ifndef FSSP_HILLCLIMBERBI_H
#define FSSP_HILLCLIMBERBI_H

#include <random>
#include "core/Solution.h"
#include "core/Automata.h"

class HillClimberBI {
public:
    HillClimberBI(std::mt19937& generator, Automata& automata);
    Solution search(int maxIteration, const Solution& x0);
private:
    std::mt19937& _generator;
    Automata& _automata;
    std::array<Solution, NB_NEIGHBORS> _neighbors;
};


#endif //FSSP_HILLCLIMBERBI_H
