//
// Created by koala on 14/12/18.
//

#ifndef FSSP_RANDOMWALK_H
#define FSSP_RANDOMWALK_H

#include <random>
#include <fstream>
#include "core/Solution.h"
#include "core/Automata.h"


class RandomWalk {
public:
    RandomWalk(std::mt19937& generator, Automata& automata);
    void walk(int nbIteration, std::ofstream& file);

private:
    std::mt19937& _generator;
    Automata& _automata;
};


#endif //FSSP_RANDOMWALK_H
