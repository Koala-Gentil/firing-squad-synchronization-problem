//
// Created by koala on 14/12/18.
//

#ifndef FSSP_EVOLUTIONARYSEARCH_H
#define FSSP_EVOLUTIONARYSEARCH_H

#include "core/Automata.h"
#include "core/Solution.h"

class EvolutionarySearch {
public:
    EvolutionarySearch(unsigned lambda, unsigned mu, std::mt19937& generator, Automata& automata);
    void search(unsigned nbIteration, Solution& result);

private:
    unsigned _lambda, _mu;
    std::mt19937& _generator;
    Automata& _automata;
};


#endif //FSSP_EVOLUTIONARYSEARCH_H
