//
// Created by koala on 14/12/18.
//

#include "EvolutionarySearch.h"
#include <algorithm>

EvolutionarySearch::EvolutionarySearch(unsigned lambda, unsigned mu, std::mt19937 &generator, Automata &automata)
    : _lambda(lambda), _mu(mu), _generator(generator), _automata(automata)
{ }

void EvolutionarySearch::search(unsigned nbIteration, Solution &result) {
    // Initialisation de la population
    std::vector<Solution> population(_lambda);
    for(Solution& solution: population) {
        solution.randomize(_generator);
    }

    // Étapes d'évolution
    for(int i = 0; i < nbIteration; ++i) {
        // Évaluation de la population
        for(Solution& solution: population) {
            _automata.calc(solution);
        }

        // Sélection
        std::sort(population.begin(), population.end(), [](const Solution& a, const Solution& b) {
            return a.fitness() > b.fitness();
        });
        std::vector<Solution> selection;
        selection.reserve(_mu);
        for(int j = 0; j < _mu; ++j) {
            selection.push_back(population[i]);
        }

        // Création de la nouvelle population
        std::uniform_int_distribution<int> randomSelection {0, selection.size() - 1};
        for(Solution& solution: population) {
            solution.combine(selection[randomSelection(_generator)], _generator);

            // mutation
            solution.randomWalk(_generator);
            solution.randomWalk(_generator);
            solution.randomWalk(_generator);
        }
    }

    for(int i = 0; i < 30; ++i) {
        Solution& solution = population[i];
        std::cout << solution.fitness() << " : " << solution << std::endl;
    }
}
