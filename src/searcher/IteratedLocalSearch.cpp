//
// Created by koala on 13/12/18.
//

#include "IteratedLocalSearch.h"

IteratedLocalSearch::IteratedLocalSearch(std::mt19937& generator, Automata& automata, int hcfiNbIterations, int nbDisturbances)
: _hcfi(generator, automata), _generator(generator), _hcfiNbIterations(hcfiNbIterations), _nbDisturbances(nbDisturbances)
{ }

void IteratedLocalSearch::search(unsigned nbIterations, Solution& result) {
    Solution hillClimbingResult;
    result.fitness(-1);
    hillClimbingResult.randomize(_generator);
    for(int i = 0; i < nbIterations; ++i) {
        hillClimbingResult.disturb(_generator, _nbDisturbances);
        _hcfi.search(hillClimbingResult, hillClimbingResult, _hcfiNbIterations);
        if (hillClimbingResult.fitness() > result.fitness()) {
            result = hillClimbingResult;
        }
    }
}
