#include "RandomSearcher.h"

RandomSearcher::RandomSearcher(std::mt19937 &generator, Automata& automata)
    : _generator(generator), _automata(automata)
{ }

void RandomSearcher::search(int nbIteration, Solution& result) {
    Solution x;
    for(int i = 0; i < nbIteration; ++i) {
        x.randomize(_generator);
        _automata.calc(x);
        if(x.fitness() > result.fitness()) {
            result = x;
        }
    }
}
