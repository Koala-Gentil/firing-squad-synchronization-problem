//
// Created by koala on 13/12/18.
//

#include "HillClimberFI.h"
#include <algorithm>

HillClimberFI::HillClimberFI(std::mt19937 &generator, Automata &automata)
        : _generator(generator), _automata(automata)
{ }

Solution HillClimberFI::search(const Solution &x0, Solution& result, int maxIteration) {
    result = x0;
    for(int i = 0; i < maxIteration; ++i) {
        result.getNeighbors(_neighbors);
        std::random_shuffle(_neighbors.begin(), _neighbors.end());
        for(Solution& neighbor: _neighbors) {
            _automata.calc(neighbor);
            if(neighbor.fitness() >= result.fitness()) {
                result = neighbor;
                break;
            }
        }
    }
    return result;
}
