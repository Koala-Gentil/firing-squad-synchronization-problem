//
// Created by koala on 13/12/18.
//

#ifndef FSSP_HILLCLIMBERFI_H
#define FSSP_HILLCLIMBERFI_H


#include <random>
#include "core/Solution.h"
#include "core/Automata.h"

class HillClimberFI {
public:
    HillClimberFI(std::mt19937& generator, Automata& automata);
    Solution search(const Solution& x0, Solution& result, int maxIteration);
private:
    std::mt19937& _generator;
    Automata& _automata;
    std::array<Solution, NB_NEIGHBORS> _neighbors;
};

#endif //FSSP_HILLCLIMBERFI_H
