//
// Created by koala on 13/12/18.
//

#include "HillClimberBI.h"

HillClimberBI::HillClimberBI(std::mt19937 &generator, Automata &automata)
    : _generator(generator), _automata(automata)
{ }

Solution HillClimberBI::search(int maxIteration, const Solution &x0) {
    Solution s(x0);
    for(int i = 0; i < maxIteration; ++i) {
        s.getNeighbors(_neighbors);
        Solution& bestNeighbor = _neighbors[0];
        for(Solution& neighbor: _neighbors) {
            _automata.calc(neighbor);
            if(neighbor.fitness() > bestNeighbor.fitness()) {
                bestNeighbor = neighbor;
            }
        }
        if(bestNeighbor.fitness() > s.fitness()) {
            s = bestNeighbor;
        } else {
            return s;
        }
    }
    return s;
}
