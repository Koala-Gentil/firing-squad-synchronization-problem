#ifndef FSSP_RANDOMSEARCHER_H
#define FSSP_RANDOMSEARCHER_H

#include <random>
#include "core/Solution.h"
#include "core/Automata.h"

class RandomSearcher {
public:
    RandomSearcher(std::mt19937& generator, Automata& automata);
    void search(int nbIteration, Solution& result);

private:
    std::mt19937& _generator;
    Automata& _automata;
};


#endif //FSSP_RANDOMSEARCHER_H
