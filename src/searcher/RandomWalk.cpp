//
// Created by koala on 14/12/18.
//

#include "RandomWalk.h"

RandomWalk::RandomWalk(std::mt19937 &generator, Automata &automata)
        : _generator(generator), _automata(automata)
{ }

void RandomWalk::walk(int nbIteration, std::ofstream &file) {
    file << "step, fitness" << std::endl;
    Solution s;
    for(int i = 0; i < nbIteration; ++i) {
        s.randomize(_generator);
        _automata.calc(s);
        file << i << ", " << s.fitness() << std::endl;
    }
}
