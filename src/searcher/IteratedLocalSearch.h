//
// Created by koala on 13/12/18.
//

#ifndef FSSP_ITERATEDLOCALSEARCH_H
#define FSSP_ITERATEDLOCALSEARCH_H

#include "core/Solution.h"
#include "core/Automata.h"
#include "searcher/HillClimberFI.h"


class IteratedLocalSearch {
public:
    IteratedLocalSearch(std::mt19937& generator, Automata& automata, int hcfiNbIterations, int nbDisturbances);
    void search(unsigned nbIterations, Solution& result);
private:
    HillClimberFI _hcfi;
    std::mt19937& _generator;
    int _hcfiNbIterations;
    int _nbDisturbances;
};


#endif //FSSP_ITERATEDLOCALSEARCH_H
