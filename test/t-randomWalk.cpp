#include <random>
#include <fstream>
#include "core/Solution.h"
#include "core/Automata.h"
#include "searcher/RandomWalk.h"

int main() {
    std::random_device rd;
    unsigned seed = rd();
    std::cout << "SEED : " << seed << std::endl;
    std::mt19937 generator(seed);

    Solution::init();
    Automata automata;

    RandomWalk walkerTexasRanger(generator, automata);

    std::ofstream file("walk.csv");
    walkerTexasRanger.walk(100000, file);

    return 0;
}