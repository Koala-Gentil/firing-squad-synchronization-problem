#include <random>
#include <fstream>
#include "core/Solution.h"
#include "core/Automata.h"
#include "searcher/IteratedLocalSearch.h"
#include "utils/SvgExporter.h"

int main() {
    std::random_device rd;
    unsigned seed = rd();
    std::cout << "SEED : " << seed << std::endl;
    std::mt19937 generator(seed);
    Solution::init();

    std::ofstream file("ils.csv", std::ios::out);

    Automata automata;
    Solution result;

    file << "nbDisturbances, nbIterationsHCFI, nbIterations, fitness" << std::endl;
    for(int nbDisturbances = 1; nbDisturbances <= 10; ++nbDisturbances) {
        std::cout << nbDisturbances << std::endl;
        for(int nbIterationsHCFI = 1000; nbIterationsHCFI < 10000; nbIterationsHCFI *= 2) {
            IteratedLocalSearch ils(generator, automata, nbIterationsHCFI, nbDisturbances);
            for(int nbIterations = 1; nbIterations <= 5; ++nbIterations) {
                for(int n = 0; n < 1; ++n) {
                    result.fitness(-1);
                    ils.search(nbIterations, result);
                    file << nbDisturbances << ", " << nbIterationsHCFI << ", " << nbIterations << ", " << result.fitness() << std::endl;
                }
            }
        }
    }

    return 0;
}