#include <random>
#include <fstream>
#include "core/Solution.h"
#include "core/Automata.h"
#include "searcher/HillClimberFI.h"

int main(int argc, char** argv) {
    std::random_device rd;
    unsigned seed = rd();
    std::cout << "SEED : " << seed << std::endl;
    std::mt19937 generator(seed);
    Solution::init();

    Automata automata;
    HillClimberFI hcfi(generator, automata);
    std::ofstream file(argv[1], std::ios::out);

    Solution x0;
    Solution result;

    file << "nbIterations, fitness" << std::endl;
    for(int i = 1; i <= 100; ++i) {
        std::cout << i << std::endl;
        const int nbIterations = 1000 * i;
        for(int j = 0; j < 1; ++j) {
            x0.randomize(generator);
            hcfi.search(x0, result, nbIterations * i);
            file << nbIterations << ", " << result.fitness() << std::endl;
        }
    }
    file.close();

    return 0;
}