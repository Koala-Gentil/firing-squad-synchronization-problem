#include <random>
#include "core/Solution.h"
#include "core/Automata.h"
#include "searcher/RandomSearcher.h"

int main() {
    std::random_device rd;
    unsigned seed = rd();
    std::cout << "SEED : " << seed << std::endl;
    std::mt19937 generator(seed);
    Solution::init();

    Automata automata;
    RandomSearcher rs(generator, automata);

    Solution result;
    rs.search(1000000, result);

    std::cout << "Fitness : " << result.fitness() << std::endl;
    std::cout << result << std::endl;

    return 0;
}