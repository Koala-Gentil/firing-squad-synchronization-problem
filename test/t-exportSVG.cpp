#include <iostream>
#include <fstream>
#include "core/Solution.h"
#include "core/Automata.h"
#include "utils/SvgExporter.h"

int main() {
    Solution::init();
    Automata automata;

    std::fstream file("../solutions/solution_5.dat", std::ios::in);
    Solution s;

    file >> s;

    automata.calc(s);
    std::cout << "Fitness : " << s.fitness() << std::endl;
    std::cout << s << std::endl;

    SvgExporter::print(s, automata, s.fitness(), "test.svg", true);

    return 0;
}
