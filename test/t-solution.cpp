#include <iostream>
#include <fstream>
#include "core/Solution.h"
#include "core/Automata.h"

int main() {
    Solution::init();
    Automata automata;

    std::fstream file("../solutions/solution_5.dat", std::ios::in);
    Solution s;

    file >> s;

    automata.calc(s);
    std::cout << "Fitness : " << s.fitness() << std::endl;
    std::cout << s << std::endl;

    auto trace = automata.getTrace(s, 5);
    for(auto line: trace) {
        for(auto value: line) {
            std::cout << value;
        }
        std::cout << std::endl;
    }

    return 0;
}
