#include <random>
#include <fstream>
#include "core/Solution.h"
#include "core/Automata.h"
#include "searcher/EvolutionarySearch.h"

int main() {
    std::random_device rd;
    unsigned seed = rd();
    std::cout << "SEED : " << seed << std::endl;
    std::mt19937 generator(seed);
    Solution::init();

    Automata automata;

    EvolutionarySearch es(15000, 200, generator, automata);

    Solution result;
    es.search(50, result);

    return 0;
}