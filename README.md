# Firing squad synchronization problem


## Utilisation
### Compilation

```bash
git clone https://gitlab.com/Koala-Gentil/firing-squad-synchronization-problem

cd firing-squad-synchronization-problem
mkdir build
cd build
cmake ..
make
```
********************************************************************************************************
### Usage
```
./t-solution
./t-exportSVG
```

## Réponse aux questions
**1. Donner la taille de l’espace de recherche et la taille du voisinage.**
    
L'automate à 5 états possèdent  
(5 * 5 * 1) + (bord droit)  
(1 * 5 * 5) + (bord gauche)  
(5 * 5 * 5)  
 = 175 règles
 
 Chaque règle peut prendre 5 valeurs, on a donc 5^175 combinaisons de règles.
 
 On peut tout de fois rèduire le nombre de règles à 85 en ne prenant pas en compte celles utilisant l'état feu, et certaines règles qui sont obligatoires et d'autres qui semblent nécessaires.
 
 De plus on ne veut pas qu'une règle génère l'état "FIRE" car à priori seul les règles 11B, B11, et 111 sont nécessaires pour générer le coup de feu.
 
 On arrive donc à un espace de recherche de 4^85.
 
 Un voisin d'une solution est une solution qui ne diffère que d'une seule règle, pour obtenir un voisin on a donc le choix parmis 85 règles à modifier, et 3 possibiliès de valeur pour la règle modifié (5 - l'état FIRE - l'état actuel).
 
 On a donc une taille du voisinage de 85 * 3.
 
 
**2. Coder la métaheuristique Hill-Climbing First-improvement.**
 
 Voir ```src/searcher/HillClimberFI.cpp```.

**3. Coder une recherche ”Iterated Local Search” (ILS).**

 Voir ```src/searcher/IteratedLocalSearch.cpp```.
 
**4. Analyser les performances de l’algorithme ILS selon les paramètres**

3 paramètres :
- Nombre d'itérations du ILS
- Nombre d'itérations dans le HCFI
- Nombre de perturbations à chaque itération du ILS


 Voir ```script/ils-analyse.r```.

**5. Développer un autre algorithme d’optimisation de votre choix.**

 Voir ```src/searcher/HillClimberBI.cpp```.
