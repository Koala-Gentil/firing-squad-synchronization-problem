csv = read.csv("hfi.csv", header = TRUE)
mean.fitness = aggregate(. ~ nbIterations, csv, mean) # mean fitness per iterations
plot(mean.fitness, type = 'l')
