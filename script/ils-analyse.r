library(akima)
csv = read.csv("ils.csv", header = TRUE)
data = csv[csv['nbIterations'] == 5,]
im <- with(data,interp(nbIterationsHCFI,nbDisturbances,fitness))

image(im$x, im$y, im$z, xlab = "nombre d'itérations", ylab = "nombre de perturbations")
